import { AComponent } from './a/a.component';
import { Routes } from '@angular/router';

export const routes: Routes = [
  { path: 'component-a', component: AComponent}
];
